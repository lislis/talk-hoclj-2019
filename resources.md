# Resources extended


- https://natureofcode.com/book/
- https://thebookofshaders.com/00/
- https://github.com/thi-ng/shadergraph
- https://github.com/overtone/shadertone
- https://www.shadertoy.com/
- https://github.com/patriciogonzalezvivo/glslViewer

Shapes
https://www.flickr.com/photos/kynd/9546075099/

easing
https://easings.net/en

OpenGL
- https://learnopengl.com/
- https://github.com/bwasty/learn-opengl-rs/blob/master/src/_1_getting_started/_3_3_shaders_class.rs
 
Overview graphics
-https://wiki.alopex.li/AGuideToRustGraphicsLibraries2019

Tutorials

- https://github.com/Rovanion/webgl-clojurescript-tutorial  
- https://medium.com/@thi.ng/clojurescript-webworkers-webgl-6c2f3c717d9e
- https://hackernoon.com/workshop-report-hi-perf-clojurescript-with-webgl-asm-js-and-emscripten-a545cca083bc
- http://blog.josephwilk.net/art/overtone-shader-visuals.html

Overview
http://radar.oreilly.com/2015/05/creative-computing-with-clojure.html

Bootstrapped CLJS
- https://code.thheller.com/blog/shadow-cljs/2017/10/14/bootstrap-support.html
- https://github.com/mhuebert/shadow-bootstrap-example

Other CLJS
https://github.com/nrepl/weasel

WebGL resources
https://www.pixijs.com/

Polar shapes!
https://brilliant.org/wiki/polar-curves/
