### What is a shader?
---
A shader is a program

that runs on your graphics card (GPU)
---
### What is so special about it?
---
Shaders are highly parallel and stateless

- We don't know what the other threads are doing
- We don't know in what order we'll be running
---
### Why \*fragment\* shaders?
---
Let's talk about the graphics pipeline!
---
The graphics pipeline "is a conceptual model that describes what steps a graphics system needs to perform to render a 3D scene to a 2D screen." [1]


<small>[1] https://en.wikipedia.org/wiki/Graphics_pipeline</small>
---
![openGL graphics pipeline illustration](https://open.gl/media/img/c2_pipeline.png)
<small>https://open.gl/drawing</small>

---
Fragment shaders are all about telling each pixel on your screen which color to display.
---
### How do we interact with the graphics pipeline?
---
### OpenGL
<small>(Open Graphics Library)</small>

is an API for the graphics pipeline.

### GLSL
<small>(OpenGL Shader Language)</small>

is its programming language.
---
### GLSL
- C-like language
- lots of build in functions for doing cool math
- optimized for vector manipulation and matrix calculation
---
## Can we write
## a shader in Clojure?

---

### Shaders in Clojure land

- [thi-ng/shadergraph](https://github.com/thi-ng/shadergraph): *WebGL/GLSL shader library & dependency framework for Clojure(Script)*

- [overtone/shadertone](https://github.com/overtone/shadertone): *A mix of www.shadertoy.com and Overtone*

- otherwise every Java game engine that offers custom shaders

---

## Quick! WebGL!

---

WebGL is your browser's API for talking to the graphics pipeline directly

---

### Shaders in browser land

- a lot of libraries with various abstraction levels in JavaScript (eg [pixi.js](https://www.pixijs.com/))
- [patriciogonzalezvivo/glslCanvas](https://github.com/patriciogonzalezvivo/glslCanvas): *Simple tool to load GLSL shaders on HTML Canvas using WebGL*

---

## FigFrag
- [figwheel-main](https://figwheel.org/) (browser repl <3) + [glslCanvas](https://github.com/patriciogonzalezvivo/glslCanvas)
- https://gitlab.com/lislis/figfrag

---

## Let's write a fragment shader!
