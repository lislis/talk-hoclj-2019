#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform float u_time;
uniform vec2 u_mouse;

uniform vec4 u_vars;

float plot(float f, float r) {
    return smoothstep(f-0.1,f,r) - smoothstep(f, f+0.03, r);
}

void main(){
    vec2 st = gl_FragCoord.xy/u_resolution.xy;
    vec3 color = vec3(0.0);

    vec2 pos = vec2(0.5)-st;

    float r = length(pos)*2.0;
    float a = atan(pos.y,pos.x);

    float cut_inner = clamp(u_mouse.x / u_resolution.x, 0.2, 0.7);

    float f_r = abs(cos(a*u_vars.x + u_time))* (u_mouse.y / u_resolution.y) + cut_inner;
    float f_g = abs(cos(a*u_vars.y + u_time))*.8 + cut_inner * sin(u_time);
    float f_b = abs(sin(a*u_vars.z + u_time * -1.0))* cut_inner + .3;

    float red =  1.0-(plot(f_r, r));
    float green = 1.0-(plot(f_g, r));
    float blue = 1.0-(plot(f_b, r));
    color = vec3(red, green, blue);

    gl_FragColor = vec4(1. - color, abs(sin(u_time)));
}
