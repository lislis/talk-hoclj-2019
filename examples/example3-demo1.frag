#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform float u_time;
uniform vec2 u_mouse;

void main(){
    vec2 st = gl_FragCoord.xy/u_resolution.xy;
    vec3 color = vec3(0.0);

    vec2 pos = vec2(0.5)-st;

    float r = length(pos)*2.0;
    float a = atan(pos.y,pos.x);

    //float f = sin(a*3.);
    float f = abs(cos(a*3. + u_time));

    color = vec3( 1.0-(smoothstep(f-0.1,f,r) - smoothstep(f, f+0.03, r)), 1.0, 1.0 );

    gl_FragColor = vec4(1. - color, 1.0);
}
