# Come with us now, on a journey through trigonometry and oscillation, to the world of the fragment shader

Talk for Heart of Clojure 2019

This is about creative coding with fragment shaders.

`slides` contain the reveal.js presentation and `examples` the fragment shader examples.

